import { dashboardConstants } from '../constants/Dashboard.constants'

const initialState = {
}

export function dashboard(state = initialState, action) {
    switch (action.type) {
        case dashboardConstants.USER_LIST_REQUEST:
        case dashboardConstants.USER_LIST_SUCCESS:
            {
                return Object.assign({}, state, action.dashboard);
            }
        default:
            {
                return state;
            }
    }
}