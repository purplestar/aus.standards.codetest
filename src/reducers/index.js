import { dashboard } from './Dashboard.reducers'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    dashboard
});

export default rootReducer