import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import Dashboard from './components/Dashboard'



export default function App() {
  return (
    <div>
      {/* <nav className="navbar navbar-light">
        <ul className="nav navbar-nav">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/category">Category</Link>
          </li>
          <li>
            <Link to="/products">Products</Link>
          </li>
        </ul>
      </nav> */}

      { /* Route components are rendered if the path prop matches the current URL */}
      <Route exact path="/" component={Dashboard} />
    </div>
  );
}