import React, { useState, useEffect } from 'react'
import './Dashboard.css'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'
import * as dashboardActions from '../actions/Dashboard.actions'
import { useSelector } from 'react-redux'

function Dashboard(props) {
  const [results, setResults] = useState([])

  useEffect(() => {
    const getResults = async () => {
      props.loadUserList()
    };
    getResults()
  }, [])

  // async function getResults() {
  //   props.loadUserList()
  // }

  const response = useSelector(state => state.dashboard)

  if (results.length === 0
    && response.data
    && Array.isArray(response.data)) {
    setResults(response.data)
  }

  if ((Array.isArray(results)
    && results.length === 0)
    || results.isLoading) {
    return (
      <div className="d-flex justify-content-center mt-5">
        <div className="spinner-border" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    )
  }
  else {
    return (
      <>
        <div className="container">
          <table className="table">
            <caption>List of users</caption>
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">First name</th>
                <th scope="col">Last name</th>
                <th scope="col">Email</th>
                <th scope="col">Gender</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {
                results.map(aResult => (
                  <tr key={aResult.id}>
                    <th scope="row">{aResult.id}</th>
                    <td>{aResult.firstName}</td>
                    <td>{aResult.lastName}</td>
                    <td>{aResult.email}</td>
                    <td>{aResult.gender}</td>
                    <td>{aResult.status ? 'Yes' : 'No'}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </>
    )
  }
}

function mapStateToProps(state) {
  return {
    dashboard: state.dashboard
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...dashboardActions
    }, dispatch);
}

const connectedDashboard = withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard))
export default connectedDashboard;