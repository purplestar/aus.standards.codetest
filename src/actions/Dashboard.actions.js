import { dashboardConstants } from '../constants/Dashboard.constants'
import { dashboardServices } from '../services/Dashboard.services'

export function loadUserList() {
    return async function loadMiddleware(dispatch, getState) {
        let isLoading = true
        dispatch(request({ isLoading }));

        dashboardServices.loadUserList()
            .then(
                dashboard => {
                    //console.log('success')
                    dispatch(success(dashboard))
                },
                error => {
                    //console.log('failed')
                    dispatch(failure(error.toString()));
                    //dispatch(alertActions.error(error.toString()));
                }
            )
            .finally(
                () => {
                    isLoading = false
                    dispatch(request({ isLoading }))
                }
            )
    }

    function request(dashboard) { return { type: dashboardConstants.USER_LIST_REQUEST, dashboard } }
    function success(dashboard) { return { type: dashboardConstants.USER_LIST_SUCCESS, dashboard } }
    function failure(error) { return { type: dashboardConstants.USER_LIST_FAILURE, error } }
}