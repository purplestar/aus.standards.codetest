import axios from 'axios'

export const dashboardServices = {
    loadUserList,
};

async function loadUserList() {
    //console.log('service!!!!!!!!!')
    return await axios.get(`https://${process.env.REACT_APP_DOMAIN_NAME}/api/v1/User/list`)
}